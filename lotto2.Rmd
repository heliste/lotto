---
title: "further analysis of lotto"
author: "Antti Heliste"
date: "27 July 2018"
output: 
  html_document:
    toc: true
    number_sections: true 
    toc_float:
      collapsed: false
      smooth_scroll: false
    css: main.css
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

#library(checkpoint)
#checkpoint("2018-07-24", checkpointLocation = tempdir())

```


In my last analysis, I disregarded the effect of number of games played for simplification. Moreover, I analysed the game only from the perspective of the player. That is why in this post I seek to study the effect of the game count on the expected return of the player and the lottery company at each jackpot size. To do this, we need to plot some surface plots. I will also limit my study to Lotto as it seems to have the simplest rules. 

Before we start, let's clarify the rules we are using:

1. Seven main numbers and one extra number are drawn from the range of 1-40. The chance of winning the jackpot is 1 in 18 643 560. 
2. One normal game costs 1 euro.
3. Jackpot is set by the lottery company. It ranges from 1 million to 14.5 million euros.
4. Prize categories 2, 3, 4 are calculated as percentage from the total pot of normal games. The percentages are 3.8%, 2.5% and 3%, respectively.
5. The jackpot and the prize pots in categories 2, 3 and 4 are shared if multiple players win them.
5. Prize categories 5 (10€) and 6 (2€) are standard. Lower categories reward nothing.

#the data set

Let's start by loading the R script and our libraries.

```{r}
knitr::read_chunk(path = 'lotto.R')
```

```{r load_packages, message = F, warning = F}
```

Then, let's load the lotto file that contains the probabilities of each prize category and plot the probabilities.

```{r load_lotto, message = F}
```

```{r plot_lotto}

```

#setting the dimensions

Let's set our dimensions for the number of players and the jackpot size and then create a data frame that contains all the combinations. We will use this data frame in turn to create an expected return matrix. The dimensions are a bit larger than their real ranges to highlight any patterns.
 
```{r set_dims}
  
```

#expected return matrix and surface plot

Let's create the function to calculate the expected return. For the shared prizes, the expected return is the following:

$$E_{\mbox{prize}} = \frac{\mbox{prize}}{E[\mbox{number of winners}]} = \frac{\mbox{share %} \cdot \mbox{total pot}}{max(n \cdot p_{\mbox{win}}, 1)}$$

, where $\mbox{share %}$ is percentage of the total pot designated to the prize category, $n$ equals the number of games and $p_{\mbox{win}}$ the probability of winning the prize.

```{r lotto_exp_func}

```

Let's loop this over the data frame to get the expected return for each game count - jackpot combination, turn the result into a matrix and then plot it with a zero matrix.

```{r lotto_exp}

```

We can also plot the data in two dimensions:

```{r lotto_exp_cont}

```

As we see, the expected return is rather similar to what I got in my earlier analysis: the expected return is mostly negative and only becomes positive when the jackpot is higher than its maximum, 14.5 million euros. We can also notice that the number of games doesn't seem to have as big a difference to the expected return as the jackpot size. The only significant differences take place at the extreme values: 

1. When the game count is over ~ 19 million, the sharing of jackpot starts to decrease the expected value.
2. When the game count is less than ~ 3 million, the pot sizes start to become proportionally smaller, decreasing the expected value.

In general, we can still conlude that playing Lotto is unprofitable.

#lotto company's perspective

If the player's make a loss while playing the Lotto, then how much does the lottery company make profit?. To analyse this, we just flip the perspective, by creating a new expected return function.

For the lottery company the expected return equals

$$E_{\mbox{gain}} = n \cdot price_{\mbox{game}} - \mbox{wins}$$

```{r lotto_gain}

```

The figures show a unsurprisingly similar pattern as in the player plot: the company makes a rather big profit out of the lottery (if we exclude other operating costs) until the jackpot starts to reach 14.5 million euros. The number of games played, however, has a significant positive effect on the expected amount gained. This happens because the maximum loss is limited by the pot sharing.

Finally, let's take a look at the return percentages.

```{r extra_analy}

```

We can see that the percentage returned increases with the pot size but is rather standard across the game count. Thus, the company just makes more money in absolute terms with more games.

#conclusions

Unsurprisingly, lottery is very unprofitable for the players and very profitable for the lottery company. The company ensures its profits with low probabilities, maximum jackpots, prize pot sharing and its rule system (I didn't take into account all the limiting rules). In addition, the lottery company also sells additional bonuses such as Lotto Plus for 50 cents, which increases the chances of winning something but in effect lowers your expected return (you can test the effect with the R code at the end of my script). Overall, no player should consider Lotto as an "investment".