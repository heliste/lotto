## ---- checkpoint ----

library(checkpoint)
checkpoint("2018-07-24", checkpointLocation = tempdir())


## ---- load_packages ----

library(readr)
library(dplyr)
library(plotly)
library(tidyr)
library(purrr)

## ---- load_probalities ----

lotto <- read_delim("lotto.txt", "\t", col_names = T)
eurojackpot <- read_delim("eurojackpot.txt", "\t", col_names = T)
vikinglotto <- read_delim("vikinglotto.txt", "\t", col_names = T)

#Calculating the probabilities since they were not given directly
vikinglotto$prob <- 1 / vikinglotto$prob

## ---- show_files ----

lotto
eurojackpot
vikinglotto

## ---- plot_files ----

p_lotto <- plot_ly(
  lotto,
  x = ~ result,
  y =  ~ log10(prob),
  marker = list(size = 10),
  mode = "markers",
  type = "scatter",
  name = "lotto",
  text = ~ paste('Prize: ', wins)
)

p_euro <- plot_ly(
  eurojackpot,
  x = ~ result,
  y = ~ log10(prob),
  marker = list(size = 10),
  mode = "markers",
  type = "scatter",
  name = "euro",
  text = ~ paste('Prize: ', wins)
)
p_viking <- plot_ly(
  vikinglotto,
  x = ~ result,
  y =  ~ log10(prob),
  marker = list(size = 10),
  mode = "markers",
  type = "scatter",
  name = "viking",
  text = ~ paste('Prize: ', wins)
)

subplot(p_lotto, p_euro, p_viking) %>%
  layout(title = "probability (log10) of each result for each game")

## ---- set_values ----

#Setting the jackpot ranges
jackpot_lotto <- seq(from = 1000000, to = 15000000, by = 250000)
jackpot_euro <- seq(from = 10000000, to = 90000000, by = 250000)
jackpot_viking <- seq(from = 3000000, to = 35000000, by = 250000)

#Setting costs
lotto_cost <- 1
euro_cost <- 2
viking_cost <- 0.8

## ---- calc_exp ----

#Defining an expected value function
expec_value <-
  function(game,
           cost,
           jrange,
           name,
           prob = "prob",
           win = "wins") {
    jackprob <- game[[prob]][1]
    exp_value_rest <-
      sum(game[[prob]][-1] * (game[[win]][-1] - cost))
    
    expvs <- jrange %>%
      as_tibble() %>%
      mutate(game = exp_value_rest + (value - cost) * jackprob)
    
    colnames(expvs) <- c("jackpot", name)
    
    expvs %>%
      gather("game", "exp_value", -jackpot)
  }

#Mapping over all games
expvs <- pmap(list(
  list(lotto, eurojackpot, vikinglotto),
  list(lotto_cost, euro_cost, viking_cost),
  list(jackpot_lotto, jackpot_euro, jackpot_viking),
  list("lotto", "euro", "viking")
), expec_value)

## ---- plot_exp ----

expected_values <- reduce(expvs, bind_rows)

expected_values %>%
  group_by(game) %>%
  plot_ly(
    x =  ~ jackpot,
    y =  ~ exp_value,
    color = ~ game,
    type = "scatter",
    mode = "markers"
  ) %>%
  layout(
    title = "expected win value of different lottery games",
    xaxis = list(title = "jackpot amount (euros)"),
    yaxis = list(title = "expected value (euros)")
  )


## ---- load_lotto ----

lotto <- read_delim("lotto.txt", "\t", col_names = T)
lotto_cost <- 1

## ---- plot_lotto ----

lotto

plot_ly(
  lotto,
  x = ~ result,
  y =  ~ log10(prob),
  marker = list(size = 10),
  mode = "markers",
  type = "scatter",
  name = "lotto",
  text = ~ paste('Prize: ', wins)
) %>%
  layout(
    title = "lotto - probability of each result",
    xaxis = list(title = "result"),
    yaxis = list(title = "probability (log10)")
  )

## ---- set_dims ----

#Set dimensions
n_games <- seq(from = 0, to = 20000000, by = 250000)
jackpot <- n_games

#Create all combinations
games <- expand.grid(n_games, jackpot) %>% as_tibble()
colnames(games) <- c("n", "jackpot")


## ---- lotto_exp_func ----

lotto_exp <- function(n, j, c, ind = T) {
  #pot
  pot <- n * c
  
  #expectedvalues
  wins <- c(
    #jackpot
    0.000000053638 * (j / max(n * 0.000000053638, 1) - c),
    #6+1
    0.000000375465 * (0.038 * pot / max(n * 0.000000375465, 1) - c),
    #6+0
    0.000012014873 * (0.025 * pot / max(n * 0.000012014873, 1) - c),
    #5+0
    0.000594736198 * (0.03 * pot / max(n * 0.000594736198, 1) - c),
    #4+0
    0.010242678973 * (10 - c),
    #3+1
    0.009311526339 * (2 - c),
    #3+0
    0.067508565961 * (0 - c),
    #2+0
    0.267333921204 * (0 - c),
    #1
    0.415852766317 * (0 - c),
    #0
    0.229143361032 * (0 - c)
  )
  if (ind) {
    sum(wins)
  } else {
    n * sum(wins)
  }
}

## ---- lotto_exp ----

#Loop over the data frame
lotto_exp_matrix <-
  pmap_dbl(list(games$n, games$jackpot, lotto_cost), lotto_exp) %>%
  matrix(
    nrow = length(n_games),
    ncol = length(jackpot),
    dimnames = list(n_games, jackpot)
  )

#Zero matrix for comparison
zero_matrix <-
  matrix(
    0L,
    nrow = length(n_games),
    ncol = length(jackpot),
    dimnames = list(n_games, jackpot)
  )

plot_ly(x = jackpot, y = n_games, showscale = FALSE) %>%
  add_surface(z =  ~ lotto_exp_matrix) %>%
  add_surface(z =  ~ zero_matrix) %>%
  layout(title = "expected return by no. games and jackpot size",
         scene = list(
           xaxis = list(title = "jackpot size"),
           yaxis = list(title = "number of games"),
           zaxis = list(title = "expected return")
         ))

## ---- lotto_exp_cont ----


plot_ly(
  x = n_games,
  y = jackpot,
  z = ~ lotto_exp_matrix,
  type = "contour",
  contours = list(start = -1,
                  end = 1,
                  size = 0.1),
  colorbar = list(title = "expected return")
) %>%
  layout(
    title = "expected return by no. games and jackpot size",
    xaxis = list(title = "jackpot size"),
    yaxis = list(title = "number of games")
  )


## ---- lotto_gain ----


lotto_gain <-
  function(n,
           j,
           c,
           gain = c("gained", "wins", "ret_perc")) {
    #pot
    pot <- n * c
    
    wins <- c(
      #jackpot (only max. jackpot can be lost)
      min(n * 0.000000053638 * j, j),
      #6+1
      min(n * 0.000000375465 * 0.038 * pot, 0.038 * pot),
      #6
      min(n * 0.000012014873 * 0.025 * pot, 0.025 * pot),
      #5
      min(n * 0.000594736198 * 0.03 * pot, 0.03 * pot),
      #4
      n * 0.010242678973 * 10,
      #3+1
      n * 0.009311526339 * 2,
      0,
      0,
      0,
      0
    )
    
    gain_arg <- match.arg(gain)
    
    if (gain_arg == "gained") {
      pot - sum(wins)
    } else if (gain_arg == "wins") {
      sum(wins)
    } else if (gain_arg == "ret_perc") {
      sum(wins) / pot
    } else {
      0
    }
    
  }


win_loss_matrix <-
  pmap_dbl(list(games$n, games$jackpot, lotto_cost), lotto_gain) %>%
  matrix(
    nrow = length(n_games),
    ncol = length(jackpot),
    dimnames = list(n_games, jackpot)
  )

plot_ly(x = jackpot, y = n_games, showscale = FALSE) %>%
  add_surface(z =  ~ win_loss_matrix) %>%
  add_surface(z =  ~ zero_matrix) %>%
  layout(title = "amount gained by no. games and jackpot size",
         scene = list(
           xaxis = list(title = "jackpot size"),
           yaxis = list(title = "number of games"),
           zaxis = list(title = "amount gained")
         ))

#contour
plot_ly(
  x = n_games,
  y = jackpot,
  z = ~ win_loss_matrix,
  type = "contour",
  contours = list(start = 0,
                  end = 20000000,
                  size = 1000000),
  colorbar = list(title = "amount gained")
) %>%
  layout(
    title = "amount gained by no. games and jackpot size",
    xaxis = list(title = "jackpot size"),
    yaxis = list(title = "number of games")
  )

## ---- extra_analy ----

ret_perc_matrix <-
  pmap_dbl(list(games$n, games$jackpot, lotto_cost, "ret_perc"),
           lotto_gain) %>%
  matrix(
    nrow = length(n_games),
    ncol = length(jackpot),
    dimnames = list(n_games, jackpot)
  )

#contour
plot_ly(
  x = n_games,
  y = jackpot,
  z = ~ ret_perc_matrix,
  type = "contour",
  contours = list(start = 0,
                  end = 1,
                  size = 0.1),
  colorbar = list(title = "return %")
) %>%
  layout(
    title = "percentage of total pot returned",
    xaxis = list(title = "jackpot size"),
    yaxis = list(title = "number of games")
  )

## ---- lotto_plus ----


#try with 20 percent of plus

lotto_plus_cost <- 1.5
percentage_plus <- 0.2

lotto_gain_plus <- function(n, j, c, pc, perc) {
  #pot - only non-plus partion of price is calculated
  collected <- n * ((1 - perc) * c + perc * pc)
  pot <- n * c
  
  wins <- c(
    #7
    min(n * 0.000000053638 * j, j),
    #6+1
    #If under 1 win expected, return the expected pot value and the excess plus wins
    if (n * 0.000000375465 < 1) {
      n * 0.000000375465 * 0.038 * pot * (1 + perc * 1 / 30 * 4)
      #Otherwise return 1 pot + the expected excess wins from successful plus games
    } else {
      0.038 * pot * (1 + perc * 1 / 30 * 4)
    },
    #6
    if (n * 0.000012014873 < 1) {
      n * 0.000012014873 * 0.025 * pot * (1 + perc * 1 / 30 * 4)
      #Otherwise return 1 pot + the expected excess wins from successful plus games
    } else {
      0.025 * pot * (1 + perc * 1 / 30 * 4)
    },
    #5
    if (n * 0.000594736198 < 1) {
      n * 0.000594736198 * 0.03 * pot * (1 + perc * 1 / 30 * 4)
      #Otherwise return 1 pot + the number of plus games
    } else {
      0.03 * pot * (1 + perc * 1 / 30 * 4)
    },
    #4
    n * 0.010242678973 * 10 * (1 + perc * 1 / 30 * 4),
    #3+1
    n * 0.009311526339 * 2 * (1 + perc * 1 / 30 * 4),
    #3+0
    n * 0.067508565961 * perc * 1 / 30 * 5,
    #2+0
    n * 0.267333921204 * perc * 1 / 30 * 5,
    #1+0
    n * 0.415852766317 * perc * 1 / 30 * 5,
    #0+0
    n * 0.229143361032 * perc * 1 / 30 * 5
  )
  collected - sum(wins)
}

win_loss_plus <-
  pmap_dbl(
    list(
      games$n,
      games$jackpot,
      lotto_cost,
      lotto_plus_cost,
      percentage_plus
    ),
    lotto_gain_plus
  )

win_loss_plus_matrix <-
  matrix(
    win_loss_plus,
    nrow = length(n_games),
    ncol = length(jackpot),
    dimnames = list(n_games, jackpot)
  )

plot_ly(
  x = n_games,
  y = jackpot,
  z = ~ win_loss_plus_matrix,
  type = "contour",
  contours = list(start = 0,
                  end = 20000000,
                  size = 1000000),
  colorbar = list(title = "amount gained (€)")
) %>%
  layout(
    title = "contour plot - no. games vs. jackpot size",
    xaxis = list(title = "jackpot size (€)"),
    yaxis = list(title = "number of games played")
  )

x <- seq(from = 0, to = 1, by = 0.2)

y <- list()

for (i in 1:11) {
  win_loss_plus <-
    pmap_dbl(list(
      games$n,
      games$jackpot,
      lotto_cost,
      lotto_plus_cost,
      (i - 1) / 10
    ),
    lotto_gain_plus)
  
  y[[i]] <- matrix(
    win_loss_plus,
    nrow = length(n_games),
    ncol = length(jackpot),
    dimnames = list(n_games, jackpot)
  )
}

plot_ly(
  x = n_games,
  y = jackpot,
  z = ~ y[[10]],
  type = "contour",
  contours = list(start = 0,
                  end = 20000000,
                  size = 1000000),
  colorbar = list(title = "amount gained (€)")
) %>%
  layout(
    title = "contour plot - no. games vs. jackpot size",
    xaxis = list(title = "jackpot size (€)"),
    yaxis = list(title = "number of games played")
  )
